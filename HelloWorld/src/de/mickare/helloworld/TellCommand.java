package de.mickare.helloworld;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TellCommand implements CommandExecutor {

	/**
	 * Checks if the sender is a player
	 * 
	 * @param sender
	 *            - that will be checked
	 * @return true if sender is player
	 */
	private boolean isPlayer(CommandSender sender) {
		return sender instanceof Player;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String alias,
			String[] args) {

		Player target = null;
		if (args.length == 0 && isPlayer(sender)) {
			target = (Player) sender;
		} else if (args.length >= 1) {
			target = Bukkit.getPlayer(args[0]);
		}

		if (target == null) {
			sender.sendMessage(ChatColor.RED + "Missing Target!");
			return true;
		}

		target.sendMessage("Hello World!");
		sender.sendMessage(ChatColor.GREEN + "Message send!");
		return true;
	}

}
