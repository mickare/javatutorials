package de.mickare.helloworld;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class HelloWorldPlugin extends JavaPlugin {

	@Override
	public void onEnable() {
		// Register Event Listener
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

		// Register Command Executors
		this.getCommand("tell").setExecutor(new TellCommand());

		// Enable completed
		this.getLogger().info("HelloWorld enabled");
	}

	@Override
	public void onDisable() {

		this.getLogger().info("HelloWorld disabled");
	}

}
